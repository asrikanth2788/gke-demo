from flask import Flask, jsonify, json
import os


app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True

@app.route('/version')
def appversion():
   response = {
   "myapplication"  : [
            {
                "version":os.environ.get("appversion"),
                "lastcommitsha":os.environ.get("lastcommitsha"),
                "description":os.environ.get("description")
            }
       ]
   }
   return(response)
