from app import appversion, app
import os


def test_appversion():
    with app.app_context():
        response = appversion()
        print(response["myapplication"])
        assert response["myapplication"][0]["version"] == os.environ.get("version")
        assert response["myapplication"][0]["lastcommitsha"] == os.environ.get("lastcommitsha")
        assert response["myapplication"][0]["description"] == os.environ.get("description")
