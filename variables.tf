variable "project" {
  default = ""
}

variable "region" {
  default = "australia-southeast1"
}

variable "zone" {
  default = "australia-southeast1-a"
}

variable "cluster" {
  default = "workshop"
}

variable "credentials" {
  default = "./phrasal-insight-292923-bb27629ef64a.json"
}

#variable "kubernetes_min_ver" {
#  default = "latest"
#}
#
#variable "kubernetes_max_ver" {
#  default = "latest"
#}
