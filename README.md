This is a simple microservice  which retuns the following output when called.
{
  "myapplication": [
    {
      "version": "v1.1.3", 
      "lastcommitsha": "dcc0ed92f48272bc4ddffab65c367dfd1f5274d2", 
      "description": "sample API"
    }
  ]
}

**Application Design**: 

1. API is exposed via External load balancer.
2. Configmap – is to store variables which will be referred/loaded in API container.
3. POD –
    API – This is where our actual application is running. This API gets the necessary values from configmap.
4. Service – Service is configured as **Loadbalancer** type. So, we will have an external load balancer created.

5. Infrastructure as Code(IaC) – This CI pipeline creates **GKE** cluster and other things needed for the cluster using **Terraform** and backend configu red to bucket in GCP.

**CI Pipeline**

**Gitlab CI** to build and deploy this application along with infra.


Application CI pipeline –

This pipeline will be triggered when something is committed to the branch
1. Pre-build – (aka Code quality checks)
    a.	basic **code quality** checks performed using **Flake**
    b.	code will be unittested using **Pytest**
2. Build –
    a.	Docker image for the application will be built
3. Test -
    a.	Another round of unit tests will be performed on the docker image built in the earlier step
4. Scan –
    a.	This is just a placeholder stage to implement image **scanning** if required
5. Release – This stage has two steps. Artefactory push and GitTag, They run parallelly.  This stage is only runs in Develop  branch.
    a.	ArtefactoryPush – After performing all the required tests and scanning’s, docker image will be pushed to **GCR (Google container Registry)** repo.
    b.	GITtag – The code will create a new **git tag**, and this application follows **Semantic versioning**.
6. Deploy –
    a.	A single node **GKE** cluster will be created in Google platform using terraform and the state file is configured in GCP bucket, so that the  subsequent runs won’t try to create the infrastructure again
    b.	Application will be deployed in GKE cluster. A **HELM** chart is used for this purpose. HELM chart creates Deployment, Configmap, Service and a Docker registry secret. Docker registry secret will be used to pull images from Google container registry
    c.	Current commitSHA and git tag will be passed as parameters to HELM chart to create configmap with these values


**Application Versioning** –

This application uses **semantic versioning (major.minor.patch)**. It retrieves and creates git tags using **GitLab APIs**.
It uses checkin commit messages to bump the application version – 
    a. If commit message contains “major:” string, then major version will be increased and minor & patch versions will be reset to 0
    b. If commit message contains “minor:” string, then minor version will be increased and patch version will be reset to 0
    c. Patch version will be increased in all other cases.

**To Run Pipeline** –

This application needs below mentioned environment variables
    a.	ZONE – zone where infrastructure/GKE cluster is created
    b.	PROJECT – GCP project
    c.	IMAGE_PULL_SECRET – config to create secret
    d.	IMAGE_PULL_SECRET_NAME – Name of the secret to be created. This will be given to POD configuration
    e.	HELM_CHART_NAME – HELM chart Name
    f.	CLUSTER_NAME – GKE cluster name to be created
    g.	DEV_GCP_CREDS – GCP serviceaccount creds given to Terraform and gcloud
    h.	ACCESS_TOKEN_VALUE – Access token to use GitLab API’s

**Assumptions or Considerations** -

1. Google Account along with project and service account is already present
2. All the variables needed are pre-configured in gitlab

