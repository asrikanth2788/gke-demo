terraform {
  backend "gcs" {
    bucket = "webapi_tfstate"
    #credentials = file("~/.ssh/gke-tf-demo-277513-e39a8f6bf07b.json")
    credentials = "./account.json"
  }
}