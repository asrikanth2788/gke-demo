From ubuntu:latest

RUN apt-get update && apt-get install -y python3 python3-pip

RUN groupadd -r apigrp -g 433 && \
    useradd -u 431 -r -m -g apigrp -d /home/apiusr -s /bin/bash -c "API User" apiusr && \
    chown -R apiusr:apigrp /home/apiusr

USER apiusr

WORKDIR /home/apiusr/

RUN mkdir WebApp && mkdir tests
COPY --chown=apiusr:apigrp requirements.txt __main__.py ./
COPY --chown=apiusr:apigrp WebApp ./WebApp/
COPY --chown=apiusr:apigrp tests ./tests/

#COPY --chown=apiusr:apigrp requirements.txt __main__.py ./
#ADD --chown=apiusr:apigrp ./WebApp ./


RUN pip3 install --user -r requirements.txt

ENV PYTHONPATH=WebApp

ENTRYPOINT [ "python3","/home/apiusr/__main__.py" ]
